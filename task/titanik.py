import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    mr_df = df[df['Name'].str.contains('Mr. ')]
    mrs_df = df[df['Name'].str.contains('Mrs. ')]
    miss_df = df[df['Name'].str.contains('Miss. ')]

    median_mr = mr_df['Age'].median()
    median_mrs = mrs_df['Age'].median()
    median_miss = miss_df['Age'].median()

    count_mr = mr_df['Age'].isnull().sum()
    count_mrs = mrs_df['Age'].isnull().sum()
    count_miss = miss_df['Age'].isnull().sum()

    result = [
        ('Mr.', count_mr, round(median_mr)),
        ('Mrs.', count_mrs, round(median_mrs)),
        ('Miss.', count_miss, round(median_miss))
    ]

    return result
